# Maintainer: Muflone http://www.muflone.com/contacts/english/

pkgname=4kyoutubetomp3
pkgver=4.1.4.4350
pkgrel=2
pkgdesc="Extract audio from YouTube, Vimeo, Facebook and other online video hosting services"
arch=('x86_64')
url="https://www.4kdownload.com/products/product-youtubetomp3"
license=('custom:eula')
makedepends=('chrpath' 'imagemagick')
#install=${pkgname}.install
source=("${pkgname}_${pkgver}_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_${pkgver%.*}_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        "fix_symlink_path.patch")
sha256sums=('00ddce88e1117c9d1e33e02977b09b2b2fd0e15d938369638adb49ab2acc5fe7'
            '03cc1d130b06c7953ee3d3c9565d22a8cf620261c5946d2b2bedf9607c07baa1'
            '1614179ef021f9577124d09183c5ab4ec24cd8b55074ca91e9d921d58d62afe4'
            '1bc2c992e21bae6c51f3176f4c3e04577b3297ea98ffc45fb56ce802423cf6cb')

prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"
    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kyoutubetomp3_desktop="[Desktop Entry]
Name=4K YouTube to MP3
Name[pt_BR]=4K YouTube para MP3
GenericName=4K YouTube to MP3
GenericName[pt_BR]=4K YouTube para MP3
Comment=Download online audio
Comment[pt_BR]=Download de áudio online
Exec=4kyoutubetomp3
Terminal=false
Type=Application
Icon=4kyoutubetomp3
Categories=AudioVideo;Network;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kyoutubetomp3_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('openssl')

    # Install files
    install -m 755 -d "${pkgdir}/usr/lib"
    cp -r "${pkgname}" "${pkgdir}/usr/lib"
    chown root.root "${pkgdir}/usr/lib/${pkgname}"

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"

    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgname}/doc/eula"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
